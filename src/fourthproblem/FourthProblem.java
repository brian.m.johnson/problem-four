package fourthproblem;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

public class FourthProblem {

	public static boolean isPalindrome(int n) {
		
		String firstToString = Integer.toString(n);
		String reverse = "";
		int firstLength = firstToString.length();

        for(int i = firstLength - 1; i >= 0; i--)
        {
            reverse = reverse + firstToString.charAt(i);
        }

		int i = 0;
			while(i < 4) {
				
				if(firstToString.charAt(i) != reverse.charAt(i)) {
					return false;
				}
				i++;
				firstLength++;
			}
				
		return true;
	}
	
	
	public static void main(String[] args) {
		boolean checkPalindrome = false;
		int sizeOfArray = 0;
		int[] firstNumber = IntStream.range(100, 1000).toArray();
		int[] secondNumber = IntStream.range(100, 1000).toArray();
		
		List<Integer> resultsArray = new ArrayList<Integer>();
		
		for (int i : firstNumber) {
			for (int n : secondNumber) {
				int finalNumber = i * n;
				checkPalindrome = isPalindrome(finalNumber);
				if(checkPalindrome) {
					resultsArray.add(finalNumber);
					sizeOfArray++;
				}
				
			}
		}
		
		Collections.sort(resultsArray);
		System.out.println("This is a Palindromic: " + NumberFormat.getInstance().format(resultsArray.get(sizeOfArray - 1)));
		
	}

}
